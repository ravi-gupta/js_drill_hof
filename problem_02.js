function getHobbies(data, age) {
    if (!Array.isArray(data) || isNaN(age)) {
      return "Input data is not valid!!";
    }
    let hobbies = data.filter((user)=>{
        return user.age===age;
    })
    .map((user)=>{
        return user.hobbies;
    })
    .flat(Infinity);
    return hobbies;
  }
  module.exports = getHobbies;
  