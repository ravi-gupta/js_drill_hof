const arrayOfObjects = require("../data");
const getHobbies = require("../problem_02");

const hobbies = getHobbies(arrayOfObjects, 30);

if (hobbies.length === 0) {
  console.log("No Data Found!!!");
} else {
  console.log(hobbies);
}