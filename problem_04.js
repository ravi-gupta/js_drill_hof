function getThirdIndexPerson(arrayOfObjects, index) {
  if (!Array.isArray(arrayOfObjects) || typeof index !== "number") {
    return "Input data is not valid!!";
  }

  if (index >= 0 && index < arrayOfObjects.length) {
    const person = arrayOfObjects[index];
    return `Name: ${person.name}, City: ${person.city}`;
  } else {
    return "Index is not valid!!!, Please provide some valid index";
  }
}
module.exports = getThirdIndexPerson;
