function getEmails(arrayOfObjects){
    if (!Array.isArray(arrayOfObjects)) {
        return "Input Data is not valid";
      }
      const emails = arrayOfObjects.map((user)=>{
        return user.email;
      })
      return emails;
}
module.exports = getEmails;