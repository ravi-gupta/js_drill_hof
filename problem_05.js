function getIndividualsAges(arrayOfObjects) {
  if (!Array.isArray(arrayOfObjects)) {
    console.log("Input data is not valid!!");
    return;
  }
  const ages = arrayOfObjects.map((user) => {
    return user.name + " -> " + user.age + " years old";
  });
  console.log(ages);
}
module.exports = getIndividualsAges;
