function getStudents(arrayOfObjects, specificCountry) {
  if (!Array.isArray(arrayOfObjects) || typeof specificCountry !== "string") {
    return "Input data is not valid!!";
  }
  let validStudent = arrayOfObjects
    .filter((user) => {
      return user.isStudent === true && user.country === specificCountry;
    })
    .map((user) => {
      return user.name;
    });
  return validStudent;
}
module.exports = getStudents;
